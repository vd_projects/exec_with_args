/*===========================================================================*\
  Copyright (c) 2015 Vlad Dobrotescu                                           
                                                                               
  Permission is hereby granted, free of charge, to any person obtaining a copy 
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights 
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
  copies of the Software, and to permit persons to whom the Software is        
  furnished to do so, subject to the following conditions:                     
                                                                               
  The above copyright notice and this permission notice shall be included in   
  all copies or substantial portions of the Software.                          
                                                                               
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE 
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN    
  THE SOFTWARE.                                                                
\*===========================================================================*/

#define  UNICODE
#define _UNICODE

#include <windows.h>
#include <wchar.h>
#include <stdio.h>
#include <locale.h>

#define PROJECT_WEB_HOME L"http://bitbucket.org/vd_projects/exec_with_args/"
#define DEFAULT_EXE_NAME L"ExecWithArgs.exe"
#define PROGRAM_VER_TEXT L"1.0.0"

#define MSGBOX_TITLE_TAG L"Exec With Args: "
#define MSGBOX_TITLE_LEN 127                       // Seems like a good value  
#define MSGBOX_NUM_CHARS 2047                      // Seems like a good value  

#define SUFFIX_NUM_CHARS 13 // The length of the suffixes defined below ...    
#define SUFFIX_TAG_CHARS 9  // The length of the suffixes without the .exe     
#define SUFFIX_EXE_CHARS 4  // The length of the .exe part of the suffixes     
#define SUFFIX_ORIG_PROG L".0riginal.exe"          // Starts with Zero, not O  
#define SUFFIX_STUB_PROG L".\U000020ACxec$tub.exe" // It shows ".�xec$tub.exe" 
#define SUFFIX_ARGS_FILE L".@utoArgs.txt"

#define MARKER_NUM_CHARS 5  // The length of the markers defined below ...     
#define MARKER_ARGS_ONLY L"(-_-)"
#define MARKER_SHOW_EXEC L"(*_*)"

#define SNOOZE_MAX_VALUE 1000
#define SNOOZE_NUM_CHARS 7

#define STRING_MAX_CHARS UNICODE_STRING_MAX_CHARS  // 32767 wchars             

enum ErrorNum {
  WIN32API_ERR = 1,
  CMD_TOO_LONG = 2,
  NO_PROG_FILE = 3,
  NO_ARGS_FILE = 4
};

int ShowHelp(void);
int ErrorMsg(ErrorNum ErrorTag, const wchar_t *ErrorTxt=NULL, UINT ErrorVal=0);

void UndoRun1(void);
void UndoRun2(void);

bool RunUndo1 = true;
bool RunUndo2 = true;

wchar_t  MsgTitle[MSGBOX_TITLE_LEN+1]; // Default message window title         
wchar_t  TextBuff[MSGBOX_NUM_CHARS+1]; // Text shown in a MessageBox           
wchar_t  ProgName[STRING_MAX_CHARS+1]; // Fully qualified name of the program  
wchar_t  ProgPath[STRING_MAX_CHARS+1]; // Path to this program (for display)   
wchar_t  StubName[STRING_MAX_CHARS+1]; // Fully qualified name of the stub     
wchar_t *StubMark;                     // Start of the renaming tag            

/*===========================================================================*\
  To allow the original program to run without any interference (exactly the   
  same way it would have been run if it wasn't instrumented), two runs of this 
  program are needed. The first run will rename this program's file, adding a  
  special tag (*.�xec$tub.exe) and execute it again with all the args (if any) 
  passed on the command line. The second run will rename the original program  
  to its original name, run it with the combined arguments obtained from the   
  args file (*.@utoArgs.txt) and from its command line, wait for the specified 
  delay (if required), and then revert all the file renaming.                  
  --------------------------------------------------------------------------   
  The args file has to be ANSI encoded, as the UTF8 encoding doesn't work well 
  in this version of MinGW (TDM 4.9.2). Only one line of this file (max. 32kB) 
  is read and used for the arguments to be injected. In most cases this is the 
  first line of the file (the \n ending is optional). This line might be the   
  second one if the first line of the file starts with the sleeping "(-_-)" or 
  puzzled "(*_*)" emoticons. The "puzzled" emoticon indicates that the user is 
  shown what command will be executed and offered the chance to cancel it (for 
  debug purposes). Either one of the two emoticons can be followed by a number 
  (an integer value capped at 1000) that indicates (in milliseconds) the delay 
  (process sleep) to be inserted between starting the the original program and 
  reverting the file renaming. If no value is specified, no sleep command will 
  be inserted (which is different from inserting 0ms sleep).                   
\*===========================================================================*/

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
  bool  FirstRun = true;
  bool  AskToRun = false;
  int   WaitTime = -1;
  UINT  DbgStyle = MB_OKCANCEL|MB_ICONQUESTION;
  FILE *ProgFile;
  FILE *ArgsFile;
  
  wchar_t  ArgsBuff[STRING_MAX_CHARS+1]; // Command line arguments (Argv[1+])  
  wchar_t  ExecLine[STRING_MAX_CHARS+1]; // Full command line to be executed   
  wchar_t  WaitText[SNOOZE_NUM_CHARS+1]; // Text value of snooze (for display) 
  wchar_t *FileName;
  wchar_t *PathItem;
  wchar_t *InitLine;
  wchar_t *InitArgs;
  wchar_t *FileArgs;
  
  size_t PQualLen;
  size_t PNameLen;
  size_t PPathLen;
  size_t IArgsLen;
  size_t FArgsLen;

  STARTUPINFO         InitInfo;
  PROCESS_INFORMATION ProcInfo;

  wcscpy(MsgTitle, DEFAULT_EXE_NAME);
  
/*===========================================================================*\
  Get and process the file name and command line used to start this program    
  ---------------------------------------------------------------------------  
  There are cases when Argv[0] of the command line can't be used as it is to   
  start the original program (for example if the original program's run uses   
  the "App Path" Windows registration). The only sure way to find this name is 
  by using the fully qualified path to this file.                              
\*===========================================================================*/

  // Get the full qualified path to this file ...                              
  PQualLen = GetModuleFileName(NULL, ProgName, STRING_MAX_CHARS);
  if (PQualLen == STRING_MAX_CHARS)
    // Highly unlikely (can only occur for the first run).                     
    return ErrorMsg(WIN32API_ERR, L"GetModuleFileName(PROG)", GetLastError());
  
  // ... figure out this file's name ...                                       
  FileName = ProgName;
  while ((PathItem = wcspbrk(FileName+1, L":\\/"))) FileName = PathItem;
  FileName++;
  PNameLen = wcslen(FileName);
  if (wcscmp(FileName, DEFAULT_EXE_NAME) == 0) return ShowHelp();
  swprintf(MsgTitle, MSGBOX_TITLE_LEN, MSGBOX_TITLE_TAG L"\"%s\"", FileName);
  PPathLen = PQualLen-PNameLen;
  wcsncpy(ProgPath, ProgName, PPathLen);
  ProgPath[PPathLen] = L'\0';
  
  // ... find where in the command line Argv[0] ends ...                       
  InitLine = GetCommandLine();
  if (InitLine[0] == L'\"') {
    InitArgs = wcschr(InitLine+1, L'\"');
    if (InitArgs != NULL) InitArgs++; // The "if" is for very unlikely cases.  
  } else {
    InitArgs = wcspbrk(InitLine, L" \t");
  }
  if (InitArgs == NULL) InitArgs = InitLine+wcslen(InitLine); // No Argv[1+].  
  IArgsLen = wcslen(InitArgs);
  
  // ... and determine if the program name ends with the stub string (2nd run).
  if (PNameLen > SUFFIX_NUM_CHARS) {
    StubMark = FileName+PNameLen-SUFFIX_NUM_CHARS;
    FirstRun = (wcscmp(StubMark, SUFFIX_STUB_PROG) != 0);
  }
  
  if (FirstRun) {
    // FIRST RUN: Prepare the buffer for the stub name ...                     
    if (PQualLen+SUFFIX_TAG_CHARS+2+IArgsLen > STRING_MAX_CHARS) 
      return ErrorMsg(CMD_TOO_LONG, L"STUB");
    wcscpy(StubName, ProgName);
    StubMark = StubName+PQualLen-SUFFIX_EXE_CHARS;
    
    // ... check if the real program exists ...                                
    wcscpy(StubMark, SUFFIX_ORIG_PROG);
    ProgFile = _wfopen(StubName, L"rb");
    if (!ProgFile) return ErrorMsg(NO_PROG_FILE, StubName+PPathLen);
    fclose(ProgFile);
    
    // ... check if the arguments file exists and can be read ...              
    wcscpy(StubMark, SUFFIX_ARGS_FILE);
    ArgsFile = _wfopen(StubName, L"rt");
    if (!ArgsFile) return ErrorMsg(NO_ARGS_FILE, StubName+PPathLen);
    
    // ... read the first few chars to see if the debug was requested ...      
    fgetws(ArgsBuff, STRING_MAX_CHARS, ArgsFile);
    fclose(ArgsFile);
    AskToRun = (wcsncmp(ArgsBuff, MARKER_SHOW_EXEC, MARKER_NUM_CHARS) == 0);
    
    // ... rename this program's file adding the stub tag ...                  
    wcscpy(StubMark, SUFFIX_STUB_PROG);
    if (_wrename(ProgName, StubName)) 
      return ErrorMsg(WIN32API_ERR, L"_wrename(PROG->STUB)", errno);
    atexit(UndoRun1);
    
    // ... display the running execution info (if required) ...                
    if (AskToRun) {
      swprintf(TextBuff, MSGBOX_NUM_CHARS,
        L"Ready to Start Second Run:\t\n\n"
        L"    PATH:\t \"%.*s\"\t\n"
        L"    FILE:\t \"%.*s\"\t\n"
        L"    ARGS:\t [%.*s]\t\n",
        MSGBOX_TITLE_LEN, ProgPath, 
        MSGBOX_TITLE_LEN, StubName+PQualLen-PNameLen, 
        MSGBOX_TITLE_LEN, InitArgs);
      if (MessageBox(NULL, TextBuff, MsgTitle, DbgStyle) == IDCANCEL) return 0;
    }
    
    // ... execute the stub with the arguments from the command line ...       
    swprintf(ExecLine, STRING_MAX_CHARS, L"\"%s\" %s", StubName, InitArgs);
    ZeroMemory(&InitInfo, sizeof(InitInfo));
    InitInfo.cb = sizeof(InitInfo);
    ZeroMemory(&ProcInfo, sizeof(ProcInfo));
    if(!CreateProcess(
        StubName,     // The fully qualified name of the stub                  
        ExecLine,     // The full stub command line                            
        NULL,         // Process handle not inheritable                        
        NULL,         // Thread handle not inheritable                         
        FALSE,        // Set handle inheritance to FALSE                       
        0,            // The priority flags of this process                    
        NULL,         // Use parent's environment block                        
        NULL,         // Use parent's starting directory                       
        &InitInfo,    // Pointer to the empty STARTUPINFO structure            
        &ProcInfo)    // Pointer to the empty PROCESS_INFORMATION structure    
      ) return ErrorMsg(WIN32API_ERR, L"CreateProcess(STUB)", GetLastError());
    
    // ... cancel the UndoRun1 and exit.                                       
    RunUndo1 = false;
    return 0;
  }
  
  // SECOND RUN: Adjust the prog name (*.�xec$tub.exe -> *.exe) ...            
  wcscpy(StubMark, L".exe");
  PQualLen = PQualLen-SUFFIX_TAG_CHARS;
  
  // ... prepare the buffer for the stub name ...                              
  wcscpy(StubName, ProgName);
  StubMark = StubName+PQualLen-SUFFIX_EXE_CHARS;
  atexit(UndoRun1);
  
  // ... check if the arguments file exists and can be read ...                
  wcscpy(StubMark, SUFFIX_ARGS_FILE);
  ArgsFile = _wfopen(StubName, L"rt");
  if (!ArgsFile) return ErrorMsg(NO_ARGS_FILE, StubName);
  
  // ... read the relevant from the args info ...                              
  fgetws(ArgsBuff, STRING_MAX_CHARS, ArgsFile);
  if (wcsncmp(ArgsBuff, MARKER_SHOW_EXEC, MARKER_NUM_CHARS) == 0) {
    // It is a (*_*) "delay" line - get the value and read the next line       
    AskToRun = true;
    swscanf(ArgsBuff+MARKER_NUM_CHARS, L" %i", &WaitTime);
    if (WaitTime > SNOOZE_MAX_VALUE) WaitTime = SNOOZE_MAX_VALUE;
    fgetws(ArgsBuff, STRING_MAX_CHARS, ArgsFile);
  } else if (wcsncmp(ArgsBuff, MARKER_ARGS_ONLY, MARKER_NUM_CHARS) == 0) {
    // It is a (-_-) "delay" line - get the value and read the next line       
    swscanf(ArgsBuff+MARKER_NUM_CHARS, L" %i", &WaitTime);
    if (WaitTime > SNOOZE_MAX_VALUE) WaitTime = SNOOZE_MAX_VALUE;
    fgetws(ArgsBuff, STRING_MAX_CHARS, ArgsFile);
  }
  fclose(ArgsFile);
  
  // ... cleanup the args and check the length of the resulting command ...    
  FileArgs = ArgsBuff;
  while (*FileArgs == L' ') FileArgs++;
  FArgsLen = wcslen(FileArgs);
  if (FArgsLen && (FileArgs[FArgsLen-1] == L'\n')) 
    FileArgs[--FArgsLen] = L'\0';
  if (PQualLen+3+FArgsLen+IArgsLen > STRING_MAX_CHARS) 
    return ErrorMsg(CMD_TOO_LONG, L"ORIGINAL");
  wcscpy(FileArgs+FArgsLen, InitArgs);
  
  // ... rename the original program to its original name ...                  
  wcscpy(StubMark, SUFFIX_ORIG_PROG);
  if (_wrename(StubName, ProgName)) 
    return ErrorMsg(WIN32API_ERR, L"_wrename(ORIG->PROG)", errno);
  atexit(UndoRun2);
  
  // ... display the running execution info (if required) ...                  
  if (AskToRun) {
    if (WaitTime >=0) {
      swprintf(WaitText, SNOOZE_NUM_CHARS, L"%i ms", WaitTime);
    } else {
      wcscpy(WaitText, L"[NONE]");
    }
    swprintf(TextBuff, MSGBOX_NUM_CHARS,
      L"Ready to Start the Real Program:\t\n\n"
      L"    PATH:\t \"%.*s\"\t\n"
      L"    FILE:\t \"%.*s\"\t\n"
      L"    ARGS:\t [%.*s]\t\n"
      L"    WAIT:\t %s\t\n",
      MSGBOX_TITLE_LEN, ProgPath, 
      MSGBOX_TITLE_LEN, FileName, 
      MSGBOX_TITLE_LEN, FileArgs, 
      WaitText);
    if (MessageBox(NULL, TextBuff, MsgTitle, DbgStyle) == IDCANCEL) return 0;
  }
  
  // ... execute the original program with all the arguments ...               
  swprintf(ExecLine, STRING_MAX_CHARS, L"\"%s\" %s", ProgName, FileArgs);
  ZeroMemory(&InitInfo, sizeof(InitInfo));
  InitInfo.cb = sizeof(InitInfo);
  ZeroMemory(&ProcInfo, sizeof(ProcInfo));
  if(!CreateProcess(
      ProgName,     // The fully qualified name of the program (no tags)       
      ExecLine,     // The full program command line                           
      NULL,         // Process handle not inheritable                          
      NULL,         // Thread handle not inheritable                           
      FALSE,        // Set handle inheritance to FALSE                         
      0,            // The priority flags of this process                      
      NULL,         // Use parent's environment block                          
      NULL,         // Use parent's starting directory                         
      &InitInfo,    // Pointer to the empty STARTUPINFO structure              
      &ProcInfo)    // Pointer to the empty PROCESS_INFORMATION structure      
    ) return ErrorMsg(WIN32API_ERR, L"CreateProcess(ORIG)", GetLastError());
  
  // ... sleep the required duration (if any) and exit.                        
  if (WaitTime >= 0) Sleep(WaitTime);
  return 0;
}

void UndoRun1(void) {
  if (!RunUndo1) return;
  wcscpy(StubMark, SUFFIX_STUB_PROG);
  _wrename(StubName, ProgName);
}

void UndoRun2(void) {
  if (!RunUndo2) return;
  wcscpy(StubMark, SUFFIX_ORIG_PROG);
  _wrename(ProgName, StubName);
}

int ErrorMsg(ErrorNum ErrorTag, const wchar_t *ErrorTxt, UINT ErrorVal) {
  switch(ErrorTag) {
    case WIN32API_ERR:
      swprintf(TextBuff, MSGBOX_NUM_CHARS,
        L"WIN32 API ERROR:\t\n\n"
        L"    %s: Error %d\t\n",
        ErrorTxt, ErrorVal);
      break;
    case CMD_TOO_LONG:
      swprintf(TextBuff, MSGBOX_NUM_CHARS,
        L"%s PROGRAM LAUNCHING ERROR:\t\n\n"
        L"    Resulting command exceeds: %d chars\t",
        ErrorTxt, STRING_MAX_CHARS);
      break;
    case NO_PROG_FILE:
      swprintf(TextBuff, MSGBOX_NUM_CHARS,
        L"CONFIGURATION ERROR: Missing original program:\t\n\n"
        L"    PATH:\t \"%.*s\"\t\n"
        L"    FILE:\t \"%.*s\"\t\n",
        MSGBOX_TITLE_LEN, ProgPath, MSGBOX_TITLE_LEN, ErrorTxt);
      break;
    case NO_ARGS_FILE:
      swprintf(TextBuff, MSGBOX_NUM_CHARS,
        L"CONFIGURATION ERROR: Missing arguments file:\t\n\n"
        L"    PATH:\t \"%.*s\"\t\n"
        L"    FILE:\t \"%.*s\"\t\n",
        MSGBOX_TITLE_LEN, ProgPath, MSGBOX_TITLE_LEN, ErrorTxt);
      break;
  }
  MessageBox(NULL, TextBuff, MsgTitle, MB_OK|MB_ICONERROR);
  return ErrorTag;
}

int ShowHelp(void) {
  MessageBox(NULL, 
    L"This program allows a set of arguments to be read from a file and "
     "\"injected\" in the command line of a program when executed, \t\n"
    L"no matter how the execution was triggered. If the execution's command "
     "line has any arguments, they will be appended to the \t\n"
    L"\"injected\" ones. To achieve this, the original program's name has to "
     "have a special tag added to it, and " DEFAULT_EXE_NAME L" has \t\n"
    L"to be renamed to the original program's name. As some programs don't "
     "accept their name to be changed, the execution of the \t\n"
    L"original program is performed in two steps. In the first step, the "
     "renamed " DEFAULT_EXE_NAME L" changes its name to a stub name, \t\n"
    L"and executes this program name, passing to it any arguments it received "
     "on its command line. In the second step, the original \t\n"
    L"program is renamed to its original name, and started with the command "
     "line arguments read from the arguments file plus those \t\n"
    L"received at execution time (if any). After the execution of the "
     "original program is started, all the file renaming is undone.\t\n"
    L"\n"
    L"To minimize the risk of name collisions between the \"instrumented\" "
     "and the original versions of the program, some exotic tags \t\n"
    L"have been used. The original program name has to be changed from *.exe "
     "to *.0riginal.exe (the \"0riginal\" tag starts with the \t\n"
    L"number zero, not with the letter 'O'). The stub name used for the "
     "second step will be *.\U000020ACxec$tub.exe. The arguments file has \t\n"
    L"to be named *.@utoArgs.txt, and has to be encoded in ANSI, as reading "
     "Unicode files (UTF-8, etc.) is not supported. \t\n"
    L"\n"
    L"To deal with programs that have complex startup procedures, the second "
     "run of " DEFAULT_EXE_NAME L" can be made to insert a \t\n"
    L"small delay (process sleep) between the start of the real program and "
     "the roll back of the file renaming. This delay is needed \t\n"
    L"when a program's initialization is re-running itself and information is "
     "passed between the runs without using the command line.\t\n"
    L"If the start-up delay is not needed, the arguments to be injected have "
     "to be in the first line of the arguments file (the leading \t\n"
    L"spaces will be trimmed and anything after that will be ignored). If a "
     "delay is needed, the arguments will go in the second line. \t\n"
    L"The first line has to start with (-_-) (the five characters "
     "\"sleeping\" emoticon), followed by the number of milliseconds of sleep "
     "\t\n"
    L"required (the value is capped to 1000). Not providing a value on this "
     "line (or giving a negative value) means that no sleep will \t\n"
    L"be inserted (which is not the same as providing a value of zero: 0 ms "
     "of sleep inserted). \t\n"
    L"\n"
    L"For debug purposes, if the first line of the arguments file starts with "
     "(*_*) (the \"puzzled\" emoticon) instead of (-_-) (followed \t\n"
    L"or not by a delay value), with the arguments being in the second line, "
     "a message box (showing the command line to be used) \t\n"
    L"will pop up before the actual execution of each step, allowing the "
     "execution to be canceled. If any of the two executions are \t\n"
    L"canceled, any file renaming already performed will be rolled back. \t\n"
    L"\n"
    L"HOW TO USE IT\n"
    L"\n"
    L"For illustration purpose, the original program to be executed is named "
     "PROGRAM.exe. \t\n"
    L"    1.  Copy " DEFAULT_EXE_NAME L" to the folder where PROGRAM.exe "
     "resides;	 \t\n"
    L"    2.  Rename PROGRAM.exe to PROGRAM.0riginal.exe (\"0riginal\" starts "
     "with number zero, not letter 'O'); \t\n"
    L"    3.  Rename " DEFAULT_EXE_NAME L" to PROGRAM.exe;	 \t\n"
    L"    4.  Create a new text file named PROGRAM.@utoArgs.txt;	 \t\n"
    L"    5.  Write the delay information (if needed) and the custom "
     "arguments on the first line(s) of this file;	 \t\n"
    L"    6.  Edit the resources of PROGRAM.exe using Resource Hacker (or a "
     "similar program), and add to it the icon group(s) from \t\n"
    L"         PROGRAM.0riginal.exe, so that Windows Explorer finds them "
     "where it knows they should be (optionally, the Description \t\n"
    L"         field of the program's \"Version Info\" resource can be "
     "changed to something relevant). \t\n"
    L"\n"
    L"USEFUL LINKS\n"
    L"\n"
    L"Project's Home:\t" PROJECT_WEB_HOME L"\t\n"
    L"Resource Hacker:\thttp://www.angusj.com/resourcehacker/\t\n"
    L"\n"
    L"        HINT: Pressing Ctrl-C when this window has focus will copy its "
     "entire contents to the clipboard, as text.\t", 
    DEFAULT_EXE_NAME L" - v" PROGRAM_VER_TEXT, 
    MB_OK|MB_ICONINFORMATION);
  return 0;
}

