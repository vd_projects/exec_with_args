# ExecWithArgs.exe

This small Windows program allows a set of arguments to be read from a file 
and "injected" in the command line of a program when executed, no matter how 
the program's execution was triggered. If the execution's command line has any 
arguments, they will be appended to the "injected" ones. To achieve this, the 
original program's name has to have a special tag added to it, and 
**ExecWithArgs.exe** has to be renamed to the original program's name. As some 
programs don't accept their name to be changed, the execution of the original 
program is performed in two steps. In the first step, the renamed 
**ExecWithArgs.exe** changes its name to a stub name, and executes this 
program name, passing to it any arguments it received on its command line. In 
the second step, the original program is renamed to its original name, and 
started with the command line arguments read from the arguments file plus 
those received at execution time (if any). After the execution of the original 
program is started, all the file renaming is undone.

-------------------------------------------------------------------------------

For a more detailed description, see the project's [Wiki][] page.

[Wiki]: https://bitbucket.org/vd_projects/exec_with_args/wiki
